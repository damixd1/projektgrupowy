﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace projekt_grupowy
{
    [Serializable]
    public class HeatMap: ISerializable
    {
        private double opacity;
        public double Opacity
        {
            get
            {
                return this.opacity;
                
            }
            set
            {
                opacity=value;
                map = null;
            }
         }

        private byte[] bitmap;
    
        [NonSerialized]
        private Image map;

        public int width { get; set; }
        public int height { get; set; }

        private PixelFormat pixelFormat;

        public byte[] getBytes()
        {
            return bitmap;
        }

        public int getByteCount()
        {
            return BitmapUtils.getByteCount(pixelFormat, height, width);
        }

        private void setImage()
        {
            map = new Image();
            map.Source = BitmapSource.Create(width, height, BitmapUtils.dpiX, BitmapUtils.dpiY, pixelFormat, null, bitmap, BitmapUtils.calcStride(pixelFormat, height, width));
            map.Opacity = opacity;
            map.Tag = "Heatmap";
        }

        public Image getImage()
        {
            if (map == null)
            {
                setImage();
            }
            return map;
        }

        public HeatMap(int height, int width, byte[] bytes, double opacity)
        {
            this.height = height;
            this.width = width;
            this.opacity = opacity;

            pixelFormat = PixelFormats.Rgb24;
            this.bitmap = bytes;
        }

        protected HeatMap(SerializationInfo info, StreamingContext context)
        {
            PixelFormatConverter pixelFormatConverter = new PixelFormatConverter();
            String pixelFs;
            opacity = info.GetDouble("opacity");
            bitmap = (byte[])info.GetValue("bitmap", typeof(byte[]));
            width = info.GetInt32("width");
            height = info.GetInt32("height");
            pixelFs = info.GetString("pixelFormat");

            pixelFormat = (PixelFormat)pixelFormatConverter.ConvertFromString(pixelFs);
        }

        public virtual void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            PixelFormatConverter pixelFormatConverter = new PixelFormatConverter();
            String pixelFS = pixelFormatConverter.ConvertToString(pixelFormat);
            info.AddValue("opacity",opacity);
            info.AddValue("bitmap",bitmap);
            info.AddValue("width",width);
            info.AddValue("height",height);
            info.AddValue("pixelFormat", pixelFS);
        }

    }
}
