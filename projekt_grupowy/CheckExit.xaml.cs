﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace projekt_grupowy
{
    /// <summary>
    /// Interaction logic for CheckExit.xaml
    /// </summary>
    public partial class CheckExit : Window
    {
        public bool yes;

        public CheckExit()
        {
            InitializeComponent();
            yes = false;
        }

        private void Button_Click(object sender, RoutedEventArgs e) //no
        {
            yes = false;
            this.Close();
        }

        private void Button_Click_1(object sender, RoutedEventArgs e) //yes
        {
            yes = true;
            this.Close();
        }
    }
}
