﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace projekt_grupowy
{
    /// <summary>
    /// Interaction logic for ScaleDialog.xaml
    /// </summary>
    public partial class ScaleDialog : Window
    {
        public double scale;
        bool warningState = false;
        public bool repeat = false;

        public ScaleDialog()
        {
            InitializeComponent();
            txtBox.Focus();
        }

        private void Window_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Return)
            {
                String txt = txtBox.Text.Replace('.', ',');
                if(Double.TryParse(txt, out scale))
                    Close();
                else
                {
                    warningState = true;
                }
            }
        }

        private void setButton_Click(object sender, RoutedEventArgs e)
        {
            //Close();
            String txt = txtBox.Text.Replace('.', ',');
            if (Double.TryParse(txt, out scale))
                Close();
            else
            {
                warningState = true;
            }
        }

        private void repeatButton_Click(object sender, RoutedEventArgs e)
        {
            repeat = true;
            Close();
        }

    }
}
