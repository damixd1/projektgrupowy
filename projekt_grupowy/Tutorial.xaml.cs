﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace projekt_grupowy
{
    /// <summary>
    /// Interaction logic for Tutorial.xaml
    /// </summary>
    public partial class Tutorial : Window
    {


       /* Uri uri1 = new Uri("pack://application:,,,/Resources/tutorial_images/heatmap.bmp");
        Uri uri2 = new Uri("pack://application:,,,/Resources/tutorial_images/heatmap2.bmp");
        Uri uri3 = new Uri("pack://application:,,,/Resources/tutorial_images/heatmap3.bmp");
        Uri uri4 = new Uri("pack://application:,,,/Resources/tutorial_images/heatmap5.bmp");*/

     //   Dictionary<int, Uri> dictionary_uri_images = new Dictionary<int, Uri>();
       // ImageBrush brush = new ImageBrush();
      //  int actual_image = 0;
        FileStream stream;
        public Tutorial()
        {
            InitializeComponent();

          /*  dictionary_uri_images.Add(0, uri1);
            dictionary_uri_images.Add(1, uri2);
            dictionary_uri_images.Add(2, uri3);
            dictionary_uri_images.Add(3, uri4);

            brush.ImageSource = new BitmapImage(uri1);
           // Canvas_Images.Background = brush;
            step_value_label.Content = "Step 1 / " + dictionary_uri_images.Count;*/
            wbSample.Width = 1500;
            wbSample.Height = 1800;
            stream = new FileStream(Properties.Settings.Default.tutor, FileMode.Open);       

            wbSample.NavigateToStream(stream);
           
            
       
            wbSample.Navigated += (a, b) => { HideScriptErrors(wbSample, true); };
            
        }
        public void HideScriptErrors(System.Windows.Controls.WebBrowser wb, bool Hide)
        {
            FieldInfo fiComWebBrowser = typeof(System.Windows.Controls.WebBrowser).GetField("_axIWebBrowser2", BindingFlags.Instance | BindingFlags.NonPublic);
            if (fiComWebBrowser == null) return;
            object objComWebBrowser = fiComWebBrowser.GetValue(wb);
            if (objComWebBrowser == null) return;
            objComWebBrowser.GetType().InvokeMember("Silent", BindingFlags.SetProperty, null, objComWebBrowser, new object[] { Hide });
        }

        private void Button_Next_Click(object sender, RoutedEventArgs e)
        {
          /*  if (dictionary_uri_images.Count -1 > actual_image)
            {
                ChangeImage(++actual_image);
                if (actual_image == dictionary_uri_images.Count - 1)
                {
                    Button_Next.IsEnabled = false;
                }
                else
                {
                    Button_Previous.IsEnabled = true;
                }
            }*/
        }

        private void Button_Previous_Click(object sender, RoutedEventArgs e)
        {
           /* if ((dictionary_uri_images.Count - 1 >= actual_image) && (actual_image != 0))
            {
                ChangeImage(--actual_image);
                if (actual_image == 0)
                {
                    Button_Previous.IsEnabled = false;
                }
                else
                {
                    Button_Next.IsEnabled = true;
                }

            }*/
        }
        private void ChangeImage(int val)
        {    
         /*   brush.ImageSource = new BitmapImage(dictionary_uri_images[val]);
           // Canvas_Images.Background = brush;
            step_value_label.Content = "Step " + (actual_image + 1) + " / " + dictionary_uri_images.Count;*/
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            stream.Close();
        }
    }
}
