﻿#pragma checksum "..\..\Options.xaml" "{406ea660-64cf-4c82-b6f0-42d48172a799}" "8989115CB31279AA32BD6F3E3E9BEBCD"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;
using projekt_grupowy;


namespace projekt_grupowy {
    
    
    /// <summary>
    /// Options
    /// </summary>
    public partial class Options : System.Windows.Window, System.Windows.Markup.IComponentConnector {
        
        
        #line 29 "..\..\Options.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Canvas Legend;
        
        #line default
        #line hidden
        
        
        #line 30 "..\..\Options.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label Lab1;
        
        #line default
        #line hidden
        
        
        #line 40 "..\..\Options.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label Lab2;
        
        #line default
        #line hidden
        
        
        #line 69 "..\..\Options.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox poor;
        
        #line default
        #line hidden
        
        
        #line 116 "..\..\Options.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox good;
        
        #line default
        #line hidden
        
        
        #line 161 "..\..\Options.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox best;
        
        #line default
        #line hidden
        
        
        #line 214 "..\..\Options.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button reset_to_default_button;
        
        #line default
        #line hidden
        
        
        #line 215 "..\..\Options.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button accept_button;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/projekt_grupowy;component/options.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\Options.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.Legend = ((System.Windows.Controls.Canvas)(target));
            return;
            case 2:
            this.Lab1 = ((System.Windows.Controls.Label)(target));
            return;
            case 3:
            this.Lab2 = ((System.Windows.Controls.Label)(target));
            return;
            case 4:
            this.poor = ((System.Windows.Controls.ComboBox)(target));
            
            #line 69 "..\..\Options.xaml"
            this.poor.SelectionChanged += new System.Windows.Controls.SelectionChangedEventHandler(this.poor_SelectionChanged);
            
            #line default
            #line hidden
            return;
            case 5:
            this.good = ((System.Windows.Controls.ComboBox)(target));
            
            #line 116 "..\..\Options.xaml"
            this.good.SelectionChanged += new System.Windows.Controls.SelectionChangedEventHandler(this.good_SelectionChanged);
            
            #line default
            #line hidden
            return;
            case 6:
            this.best = ((System.Windows.Controls.ComboBox)(target));
            
            #line 161 "..\..\Options.xaml"
            this.best.SelectionChanged += new System.Windows.Controls.SelectionChangedEventHandler(this.best_SelectionChanged);
            
            #line default
            #line hidden
            return;
            case 7:
            this.reset_to_default_button = ((System.Windows.Controls.Button)(target));
            
            #line 214 "..\..\Options.xaml"
            this.reset_to_default_button.Click += new System.Windows.RoutedEventHandler(this.reset_to_default_click);
            
            #line default
            #line hidden
            return;
            case 8:
            this.accept_button = ((System.Windows.Controls.Button)(target));
            
            #line 215 "..\..\Options.xaml"
            this.accept_button.Click += new System.Windows.RoutedEventHandler(this.accept_click);
            
            #line default
            #line hidden
            return;
            }
            this._contentLoaded = true;
        }
    }
}

