﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace projekt_grupowy
{
    /// <summary>
    /// Represents wireless network which was detected in each interval
    /// </summary>
    [Serializable]
    public class Network
    {
        /// <summary>
        /// Network SSID
        /// </summary>
        public string Ssid { get; set; }

        /// <summary>
        /// Type of network e.g. Infrastructe
        /// </summary>
        public string NetworkType { get; set; }

        /// <summary>
        /// Type of network authentication e.g CCMP(AES)
        /// </summary>
        public string Authentication { get; set; }

        /// <summary>
        /// Type of network encryption e.g. WEP
        /// </summary>
        public string Encryption { get; set; }

        /// <summary>
        /// Network BSSID (unique for each network)
        /// </summary>
        public string Bssid { get; set; }

        /// <summary>
        /// Current network signal
        /// </summary>
        public double SignalStrength { get; set; }

        /// <summary>
        /// Type of network radio e.g. 802.11g
        /// </summary>
        public string RadioType { get; set; }

        /// <summary>
        /// Frequency channel at which network operates
        /// </summary>
        public int Channel { get; set; }

        /// <summary>
        /// Vendor of wireless network access point 
        /// </summary>
        public string Vendor { get; set; }

        /// <summary>
        /// Frequency at which network operates
        /// </summary>
        public int Freq { get; set; }

        /// <summary>
        /// Constructs Network
        /// </summary>
        public Network()
        {
        }

        /// <summary>
        /// Determines whether this instance and another specified Networking.Network object have the same value
        /// </summary>
        /// <param name="other">The Network to compare with this instance</param>
        /// <returns>true if the value of the value parameter is the same as this instance; otherwise, false.</returns>
        public bool Equals(Network other)
        {
            return Bssid.Equals(other.Bssid);
        }

        /// <summary>
        /// Converts Network to its string representation (SSID)
        /// </summary>
        /// <returns>String representation of Network (SSID)</returns>
        public override string ToString()
        {
            return Ssid;
        }
    }
}
