﻿using System;
using System.Collections.Generic;
using System.Device.Location;
using System.Linq;
using System.Runtime.InteropServices;
using System.Security.Permissions;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows;

namespace projekt_grupowy
{
    [PermissionSet(SecurityAction.Demand, Name = "FullTrust")]
    [ComVisible(true)]
    public class ScriptingObject
    {
        private FindLocationInGoogleAPIWindow context;
        public ScriptingObject(FindLocationInGoogleAPIWindow context)
        {
            this.context = context;
        }

        public void metoda(string center, string bounds, int zoom)
        {
            center = center.Replace("(", "");
            center = center.Replace(")", "");
            center = Regex.Replace(center, @"\s+", "");
            center = center.Replace(",", ":");
            center = center.Replace(".", ",");
            string[] centerCoordinates = center.Split(':');

            bounds = bounds.Replace("(", "");
            bounds = bounds.Replace(")", "");
            bounds = Regex.Replace(bounds, @"\s+", "");
            bounds = bounds.Replace(",", ":");
            bounds = bounds.Replace(".", ",");
            string[] boundsCoordinates = bounds.Split(':');

            GoogleMapManager mapManager = new GoogleMapManager();
            mapManager.CenterGeo = new GeoCoordinate(double.Parse(centerCoordinates[0]), double.Parse(centerCoordinates[1]));
            mapManager.LeftBottomGeo = new GeoCoordinate(double.Parse(boundsCoordinates[0]), double.Parse(boundsCoordinates[1]));
            mapManager.RightTopGeo = new GeoCoordinate(double.Parse(boundsCoordinates[2]), double.Parse(boundsCoordinates[3]));

            mapManager.LeftTopGeo = new GeoCoordinate(double.Parse(boundsCoordinates[2]), double.Parse(boundsCoordinates[1]));
            mapManager.RightBottomGeo = new GeoCoordinate(double.Parse(boundsCoordinates[0]), double.Parse(boundsCoordinates[3]));
            mapManager.Zoom = zoom;

            mapManager.CenterPixels = mapManager.GeoToPixels(mapManager.CenterGeo, mapManager.Zoom);
            mapManager.LeftBottomPixels = mapManager.GeoToPixels(mapManager.LeftBottomGeo, mapManager.Zoom);
            mapManager.RightTopPixels = mapManager.GeoToPixels(mapManager.RightTopGeo, mapManager.Zoom);
            mapManager.LeftTopPixels = mapManager.GeoToPixels(mapManager.LeftTopGeo, mapManager.Zoom);
            mapManager.RightBottomPixels = mapManager.GeoToPixels(mapManager.RightBottomGeo, mapManager.Zoom);

            context.GoToHeatMap(mapManager);
        }
    }
}
