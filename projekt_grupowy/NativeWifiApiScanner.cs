﻿using NativeWifi;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;

namespace projekt_grupowy
{
    /// <summary>
    /// Scanner retrieving information about surrounding wireless networks
    /// </summary>
    public sealed class NativeWifiApiScanner
    {
        /// <summary>
        /// Instance of our singleton scanner
        /// </summary>
        private static readonly NativeWifiApiScanner instance = new NativeWifiApiScanner();

        /// <summary>
        /// Scanner client
        /// </summary>
        private readonly WlanClient client;

        /// <summary>
        /// Dictionary containing router vendors
        /// </summary>
        private readonly Dictionary<string, string> vendors;

        /// <summary>
        /// Vendors resource file contains name of vendors after this word
        /// </summary>
        private const string HexSeparator = "hex";

        /// <summary>
        /// Lock variable to create critical section
        /// </summary>
        private object Lock = new object();

        /// <summary>
        /// Property to get list of scaned networks
        /// </summary>
        List<Network> _scanedNetworks;
        public List<Network> ScanedNetworks
        {
            get
            {
                lock (Lock)
                {
                    return this._scanedNetworks;
                }
            }
            set
            {
                lock (Lock)
                {
                    this._scanedNetworks = value;
                }
            }
        }

        /// <summary>
        /// Constructs NativeWifiApiScanner (singleton)
        /// </summary>
        private NativeWifiApiScanner()
        {
            client = new WlanClient();
            Thread networkScannerThread = new Thread(new ThreadStart(ScanForNetworks));
            networkScannerThread.IsBackground = true;
            networkScannerThread.Start();
        }

        /// <summary>
        /// Gets an singleton instance
        /// </summary>
        public static NativeWifiApiScanner Instance
        {
            get { return instance; }
        }

        /// <summary>
        /// Method for thread which scans for networks whole time in background
        /// </summary>
        public void ScanForNetworks()
        {
            try
            {
                while (true)
                {
                    ScanedNetworks = NativeWifiApiScanner.Instance.GetListOfDetectedNetworks();
                    Console.WriteLine("=============================\nScaned '{0}'\n=============================", DateTime.Now);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("An error occurred: '{0}'", ex);
            }
        }

        /// <summary>Gets information from all wlan interfaces and returns list of all detected WiFi networks</summary>
        /// <returns>The list of detected netowrks</returns>
        public List<Network> GetListOfDetectedNetworks()
        {
            //List to collect all data about networks from wlanBssEntries and wlanAvailableNetwork
            var DetectedNetworks = new List<Network>();

            WlanClient.WlanInterface[] interfaces = client.Interfaces;

            // If no intefaces, then exit program.
            if (interfaces == null || interfaces.Length == 0)
            {
                MessageBox.Show("Plug wireless network interface adapter");
                Environment.Exit(0);
            }

            // Check all wifi interfaces
            foreach (WlanClient.WlanInterface wlanIface in client.Interfaces)
            {
                try
                {
                    // Scan for WiFi networks
                    wlanIface.Scan();
                }
                catch (Exception e)
                {
                    MessageBox.Show(e.Message);
                    Environment.Exit(0);
                }

                // Wait to complete networks scan
                Thread.Sleep(3000);

                // Gets two array of structures with diffrent data, which need to be merged to one list
                Wlan.WlanBssEntry[] wlanBssEntries = wlanIface.GetNetworkBssList();
                Wlan.WlanAvailableNetwork[] wlanAvailableNetwork = wlanIface.GetAvailableNetworkList(0);

                // Gets information about one network
                foreach (Wlan.WlanBssEntry network in wlanBssEntries)
                {
                    var Net = new Network();
                    Net.Ssid = GetStringForSsid(network.dot11Ssid);
                    Net.Bssid = BitConverter.ToString(network.dot11Bssid);
                    Net.Channel = FrequencyToChannel(network.chCenterFrequency);
                    Net.NetworkType = network.dot11BssType.ToString();
                    Net.SignalStrength = network.rssi;
                    Net.RadioType = GetRadioType(network.dot11BssPhyType);
                    Net.Freq = (int)(network.chCenterFrequency / 1000); //in Mhz

                    // Check if MAC of this network exists in vendors list
                    //if (vendors.ContainsKey(Net.Bssid.Substring(0, 8)))
                    //{
                    //    Net.Vendor = vendors[Net.Bssid.Substring(0, 8)];
                    //}
                    //else
                    //{
                    //    Net.Vendor = "Unknown";
                    //}

                    foreach (Wlan.WlanAvailableNetwork network2 in wlanAvailableNetwork)
                    {
                        string ssid = GetStringForSsid(network2.dot11Ssid);

                        if (ssid == Net.Ssid)
                        {
                            Net.Authentication = CreateAuthenticationString(network2.dot11DefaultAuthAlgorithm);
                            Net.Encryption = CreateEncryptionString(network2.dot11DefaultCipherAlgorithm);
                        }
                    }

                    // Add network to list
                    DetectedNetworks.Add(Net);
                }
            }

            return DetectedNetworks;
        }

        /// <summary>Converts SSID network from byte array to string</summary>
        /// <param name="ssid">Struct of Wlan.Dot11Ssid with SSID array </param>
        /// <returns>The string name SSID of a network</returns>
        public static string GetStringForSsid(Wlan.Dot11Ssid ssid)
        {
            return Encoding.ASCII.GetString(ssid.SSID, 0, (int)ssid.SSIDLength);
        }

        /// <summary>Converts a center channel frequency value to a designated WiFi channel</summary>
        /// <param name="frequency">Center channel frequency to convert to a channel</param>
        /// <returns>The WiFi channel at the given center frequency</returns>
        public static int FrequencyToChannel(uint frequency)
        {
            uint channel = 0;
            // 2.4 GHz
            if ((frequency > 2400000) && (frequency < 2484000))
            {
                channel = (frequency - 2407000) / 5000;
            }

            // Channel 14 for 2.4 GHz
            if ((frequency >= 2484000) && (frequency <= 2495000))
            {
                channel = 14;
            }

            // 5 GHz
            if ((frequency > 5000000) && (frequency < 5900000))
            {
                channel = (frequency - 5000000) / 5000;
            }

            return (int)channel;
        }

        /// <summary>Creates a readable string based on the authentication method</summary>
        /// <param name="authentication">authentication used</param>
        /// <returns>String representing the authentication mode</returns>
        public static string CreateAuthenticationString(Wlan.Dot11AuthAlgorithm authentication)
        {
            string text = "";

            switch (authentication)
            {
                case Wlan.Dot11AuthAlgorithm.IEEE80211_Open:
                    text = "Open";
                    break;
                case Wlan.Dot11AuthAlgorithm.IEEE80211_SharedKey:
                    text = "WEP";
                    break;
                case Wlan.Dot11AuthAlgorithm.IHV_End:
                    break;
                case Wlan.Dot11AuthAlgorithm.IHV_Start:
                    break;
                case Wlan.Dot11AuthAlgorithm.RSNA:
                    text = "WPA2-Enterprise";
                    break;
                case Wlan.Dot11AuthAlgorithm.RSNA_PSK:
                    text = "WPA2-Personal";
                    break;
                case Wlan.Dot11AuthAlgorithm.WPA:
                    text = "WPA";
                    break;
                case Wlan.Dot11AuthAlgorithm.WPA_None:
                    text = "WPA-None";
                    break;
                case Wlan.Dot11AuthAlgorithm.WPA_PSK:
                    text = "WPA-Personal";
                    break;
                default:
                    break;
            }
            return text;
        }

        /// <summary>Creates a readable string based on the encryption algorithm</summary>
        /// <param name="encryption">encryption used</param>
        /// <returns>String representing the authentication mode</returns>
        public static string CreateEncryptionString(Wlan.Dot11CipherAlgorithm encryption)
        {
            string text = "";

            switch (encryption)
            {
                case Wlan.Dot11CipherAlgorithm.CCMP:
                    text = "CCMP(AES)";
                    break;
                case Wlan.Dot11CipherAlgorithm.IHV_End:
                    break;
                case Wlan.Dot11CipherAlgorithm.IHV_Start:
                    break;
                case Wlan.Dot11CipherAlgorithm.None:
                    text = "None";
                    break;
                case Wlan.Dot11CipherAlgorithm.RSN_UseGroup:
                    break;
                case Wlan.Dot11CipherAlgorithm.TKIP:
                    text = "TKIP";
                    break;
                case Wlan.Dot11CipherAlgorithm.WEP:
                    text = "WEP";
                    break;
                case Wlan.Dot11CipherAlgorithm.WEP104:
                    break;
                case Wlan.Dot11CipherAlgorithm.WEP40:
                    break;
                default:
                    break;
            }
            return text;
        }

        /// <summary>Gets 802.11 radio physical connection</summary>
        /// <param name="type">Type of physical radio connection</param>
        /// <returns>String representing the 802.11 radio type</returns>
        public static string GetRadioType(Wlan.Dot11PhyType type)
        {
            string text = "";

            switch (type)
            {
                case Wlan.Dot11PhyType.Any:
                    break;
                case Wlan.Dot11PhyType.DSSS:
                    break;
                case Wlan.Dot11PhyType.ERP:
                    text = "802.11g";
                    break;
                case Wlan.Dot11PhyType.FHSS:
                    break;
                case Wlan.Dot11PhyType.HRDSSS:
                    text = "802.11b";
                    break;
                case Wlan.Dot11PhyType.HT:
                    text = "802.11n";
                    break;
                case Wlan.Dot11PhyType.IHV_End:
                    break;
                case Wlan.Dot11PhyType.IHV_Start:
                    break;
                case Wlan.Dot11PhyType.IrBaseband:
                    break;
                case Wlan.Dot11PhyType.OFDM:
                    text = "802.11a";
                    break;
                case Wlan.Dot11PhyType.VHT:
                    text = "802.11ac";
                    break;
                default:
                    break;
            }
            return text;
        }

    }
}
