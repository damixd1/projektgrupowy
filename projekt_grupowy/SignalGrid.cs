﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace projekt_grupowy
{
    [Serializable]
    public class SignalGrid
    {
        public int xlengthReal { get; set; }
        public int ylengthReal { get; set; }

        public int xlength { get; set; }
        public int ylength { get; set; }

        public double pixelWidth { get; set; }

        public Dictionary<Point,float> measureReference { get; private set; }

        public SignalGrid(int xlengthR, int ylengthR)
        {
            this.measureReference = new Dictionary<Point,float>();

            this.xlengthReal = xlengthR;
            this.ylengthReal = ylengthR;

            this.xlength = (int)Math.Ceiling((double)xlengthR);
            this.ylength = (int)Math.Ceiling((double)ylengthR);
        }

        public void Add(float measure, int xR, int yR)
        {
            int x = xR;
            int y = yR;

            Point newP=new Point(){X = x, Y = y};
            AddByPoint(measure, newP);
        }

        public void AddByPoint(float measure, Point p)
        {
            if (!measureReference.ContainsKey(p))
                measureReference.Add(p, measure);
        }

        public void RemoveByPoint(Point p)
        {
            if (measureReference.ContainsKey(p))
                measureReference.Remove(p);
        }

        public void ClearMeasures()
        {
            measureReference.Clear();
        }
    }
}
