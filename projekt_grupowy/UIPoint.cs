﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using System.Windows.Shapes;

namespace projekt_grupowy
{
    [Serializable]
    class UIPoint : ISerializable
    {
        Ellipse shape;
        Color color;

        public int X { get; set; }
        public int Y { get; set; }

        public UIPoint(int x, int y, double radiusX, double radiusY, Color color, double thickness)
        {
            shape = new Ellipse() { Margin = new Thickness(x - radiusX, y - radiusY, 0, 0), Width = 2 * radiusX, Height = 2 * radiusY, Fill = new SolidColorBrush(color), StrokeThickness = thickness };

            this.color = color;
            X = x;
            Y = y;
        }

        public override bool Equals(object obj)
        {
            return Equals(obj as UIPoint);
        }

        public bool Equals(UIPoint obj)
        {
            return obj != null && obj.X == this.X && obj.Y == this.Y;
        }

        public Ellipse getUIreference()
        {
            return shape;
        }

        public Point getPoint()
        {
            return new Point() { X = this.X, Y = this.Y };
        }

        protected UIPoint(SerializationInfo info, StreamingContext context)
        {
            Color c;
            double thick, left, top, width, height;
            byte R, G, B;
            R = info.GetByte("colorR");
            G = info.GetByte("colorG");
            B = info.GetByte("colorB");
            thick = info.GetDouble("thick");
            left = info.GetDouble("left");
            top = info.GetDouble("top");
            width = info.GetDouble("width");
            height = info.GetDouble("height");
            X = info.GetInt32("x");
            Y = info.GetInt32("y");

            c=Color.FromRgb(R,G,B);
            color = c;
            shape = new Ellipse() { Margin = new Thickness(left, top, 0, 0), Width = width, Height = height, Fill = new SolidColorBrush(c), StrokeThickness = thick };
        }

        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            double thick = shape.StrokeThickness;
            info.AddValue("colorR", color.R);
            info.AddValue("colorG", color.G);
            info.AddValue("colorB", color.B);
            info.AddValue("thick", thick);
            info.AddValue("left",shape.Margin.Left);
            info.AddValue("top",shape.Margin.Top);
            info.AddValue("width", shape.Width);
            info.AddValue("height", shape.Height);
            info.AddValue("x", X);
            info.AddValue("y", Y);
        }
    }
}
