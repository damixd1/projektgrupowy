﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Drawing;
using System.Drawing.Imaging;
using System.Windows.Forms;
using System.Windows.Interop;
using System.Diagnostics;
using System.IO;
using System.Reflection;


namespace projekt_grupowy
{
    /// <summary>
    /// Interaction logic for FindLocationInGoogleAPIWindow.xaml
    /// </summary>
    public partial class FindLocationInGoogleAPIWindow : Window
    {
        public ScriptingObject scriptingObject;
        private FileStream stream;
        private bool show;

        public FindLocationInGoogleAPIWindow()
        {
            InitializeComponent();
            show = true;

            stream = new FileStream(Properties.Settings.Default.index, FileMode.Open);

            wbSample.NavigateToStream(stream);

            scriptingObject = new ScriptingObject(this);
            wbSample.ObjectForScripting = scriptingObject;

            wbSample.Navigated += (a, b) => { HideScriptErrors(wbSample, true); };
        }

        
        public void HideScriptErrors(System.Windows.Controls.WebBrowser wb, bool Hide)
        {
            FieldInfo fiComWebBrowser = typeof(System.Windows.Controls.WebBrowser).GetField("_axIWebBrowser2", BindingFlags.Instance | BindingFlags.NonPublic);
            if (fiComWebBrowser == null) return;
            object objComWebBrowser = fiComWebBrowser.GetValue(wb);
            if (objComWebBrowser == null) return;
            objComWebBrowser.GetType().InvokeMember("Silent", BindingFlags.SetProperty, null, objComWebBrowser, new object[] { Hide });
        }

        private static BitmapSource CopyScreen()
        {
            int extraTop = 23, extraRight = 15, extraBottom = 40;
            var left = Screen.AllScreens.Min(screen => screen.Bounds.X);
            var top = Screen.AllScreens.Min(screen => screen.Bounds.Y) + extraTop;
            var right = Screen.AllScreens.Max(screen => screen.Bounds.X + screen.Bounds.Width) - extraRight;
            var bottom = Screen.AllScreens.Max(screen => screen.Bounds.Y + screen.Bounds.Height) - extraBottom;
            var width = right - left;
            var height = bottom - top;

            using (var screenBmp = new Bitmap(width, height, System.Drawing.Imaging.PixelFormat.Format32bppArgb))
            {
                using (var bmpGraphics = Graphics.FromImage(screenBmp))
                {
                    bmpGraphics.CopyFromScreen(left, top, 0, 0, new System.Drawing.Size(width, height));
                    return Imaging.CreateBitmapSourceFromHBitmap(
                        screenBmp.GetHbitmap(),
                        IntPtr.Zero,
                        Int32Rect.Empty,
                        BitmapSizeOptions.FromEmptyOptions());
                }
            }
        }

        public void GoToHeatMap(GoogleMapManager mapManager)
        {
            BitmapSource bitmap = CopyScreen();
            ImageBrush map = new ImageBrush();
            map.ImageSource = bitmap;
            map.Opacity = 1;

            stream.Close();
            show = false;

            MainWindow sd = new MainWindow(map, mapManager, State.GpsMeasurement);
            sd.Show();
            this.Close();
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (show == true)
            {
                StartWindow sw = new StartWindow();
                sw.Show();
            }

            stream.Close();
        }

    }
}
