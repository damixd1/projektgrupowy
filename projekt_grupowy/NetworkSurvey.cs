﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Media.Imaging;

namespace projekt_grupowy
{
    [Serializable]
    class NetworkSurvey
    {
        public SignalGrid signalGrid { get; set; }
        public HeatMap heatmap { get; set; }
        public Network network { get; set; }

        public NetworkSurvey(Network network, SignalGrid signalGrid)
        {
            this.network = network;
            this.signalGrid = signalGrid;
        }
    }
}
