﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace projekt_grupowy
{
    /// <summary>
    /// Interaction logic for Warning.xaml
    /// </summary>
    public partial class Warning : Window
    {
        public Warning(string value)
        {
            InitializeComponent();
            komunikat.Content = value;
            var image = new Image
            {
                Source = new BitmapImage(
                    new Uri(
                        "pack://application:,,,/Resources/tutorial_images/warn3.png"))
            };
            image_warn.Source = image.Source;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
